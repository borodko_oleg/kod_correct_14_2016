$(document).ready(function() {
	
	$(".hours").each(function(indx){
		
		//next()
		element_=$(this).children('tfoot').children('tr').children('th').next();
		
		index_column=$(this).attr('Total_column');
		
		$(this).DataTable( {  //<-hours class
    "sPaginationType": "full_numbers",
    "aaSorting": [[ 0, "desc" ]],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
			
            total = api
                .column( index_column )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( index_column, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
                
             // Update footer
            $( element_ ).html(
                pageTotal.toFixed(2) //<-toFixed(2)
                
            // Update footer
            //$( api.column( 3 ).footer() ).html(
            //    '$'+pageTotal.toFixed(2) +' ( $'+ total.toFixed(2) +' total)' //<-toFixed(2)
            );
        }
    } );
	
	});
	
	
	
	
    /* --->> 1 - Datatable setup --------------*/
    oTable = $('table.advanced').dataTable({
        "sPaginationType": "full_numbers"
    });

    /* --->> 2 - Button styling setup --------------*/
    $(".dataTables_length select").uniform();

    /* --->> 3 - Column resize setup --------------*/
    $("#resizableTable").colResizable();

    /* --->> 4 - Table sorting setup --------------*/
    $("#sortableTable").tablesorter();
} );